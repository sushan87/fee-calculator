package com.sapient.assignment.transaction;

import static org.junit.Assert.assertTrue;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Spy;
import org.mockito.junit.MockitoJUnitRunner;

import com.sapient.assignment.constant.Constant.TRANSACTIONTYPE;
import com.sapient.assignment.util.Utils;

@RunWith(MockitoJUnitRunner.class)
public class TransactionTest {

	@Spy
	Transaction transaction;

	@Test
	public void compareTest() {

		Transaction t1 = new Transaction();

		Transaction t2 = new Transaction();

		t1.setClientId("a");
		t1.setTransactionType(TRANSACTIONTYPE.BUY.getType());
		t1.setPriority(true);
		t1.setTransactionDate(Utils.parseDate("12/25/2018"));

		t2.setClientId("a");
		t2.setTransactionType(TRANSACTIONTYPE.BUY.getType());
		t2.setPriority(true);
		t2.setTransactionDate(Utils.parseDate("12/25/2018"));
		assertTrue(transaction.compare(t1, t2) == 0);

		t2.setClientId("b");

		assertTrue(transaction.compare(t1, t2) != 0);

	}

}