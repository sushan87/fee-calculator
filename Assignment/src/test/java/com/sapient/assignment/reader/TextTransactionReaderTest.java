package com.sapient.assignment.reader;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.verify;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Spy;
import org.mockito.junit.MockitoJUnitRunner;

import com.sapient.assignment.constant.Constant.FILETYPE;

@RunWith(MockitoJUnitRunner.class)
public class TextTransactionReaderTest {
	
	@Spy
	ExcelTransactionReader excelTransactionReader;
	
	@Test
	public void readFileTest(){
		excelTransactionReader.readFile(FILETYPE.TEXT);
		ArgumentCaptor<FILETYPE> passwordCaptor = ArgumentCaptor.forClass(FILETYPE.class);
		verify(excelTransactionReader).readFile(passwordCaptor.capture());
		assertEquals(FILETYPE.TEXT, passwordCaptor.getValue());
		
}
}
