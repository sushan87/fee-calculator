package com.sapient.assignment;

import java.io.IOException;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;
import org.springframework.stereotype.Component;

import com.sapient.assignment.constant.Constant.FILETYPE;
import com.sapient.assignment.reader.ITransactionManager;
import com.sapient.assignment.reader.TransactionReader;

@SpringBootApplication
@Component
public class AssignmentApplication {

	@Autowired
	private ResourceLoader resourceLoader;
	
	private static ResourceLoader staticResourceLoader;
	
	@PostConstruct
	public void init(){
		staticResourceLoader = resourceLoader;
	}

	public static void main(String[] args) {
		
		SpringApplication.run(AssignmentApplication.class, args);
		Resource res = staticResourceLoader.getResource("classpath:SampleData.txt");
		TransactionReader transactionReader = TransactionReader.getTrasactionReaderInstance();
		ITransactionManager iTransactionManager = null;
		try {
			iTransactionManager = transactionReader.readFile(FILETYPE.CSV, res.getFile());
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		iTransactionManager.displayTransactionReport();
	}

}
