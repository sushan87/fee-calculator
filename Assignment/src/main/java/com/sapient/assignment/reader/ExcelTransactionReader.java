package com.sapient.assignment.reader;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

import com.sapient.assignment.constant.Constant.FILETYPE;
import com.sapient.assignment.transaction.Transaction;




public class ExcelTransactionReader extends AbstractTransactionReader implements ITransactionManager {

   
	@Override
	public void readTransaction(File transactionFile) {

		
		BufferedReader br = null;
		String line = "";
		String csvSplitBy = ",";

		try {

			br = new BufferedReader(new FileReader(transactionFile));
			while ((line = br.readLine()) != null) {
				String[] transactionAttributes = line.split(csvSplitBy);
				Transaction transaction = getTransaction(transactionAttributes); 
				saveTransaction(transaction);

			}		 
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (br != null) {
				try {
					br.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	}	


	@Override
	public ITransactionManager readFile(FILETYPE fileType) {
		if(fileType == FILETYPE.CSV){
			return TransactionReader.getTrasactionReaderInstance().readExcelFile();
		}
		return null;
	}


}
