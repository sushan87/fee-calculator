package com.sapient.assignment.reader;

import java.io.File;

import com.sapient.assignment.constant.Constant.FILETYPE;




public class XMLTransactionReader extends AbstractTransactionReader implements ITransactionManager {

	
	@Override
	public void readTransaction(File transactionFile) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public ITransactionManager readFile(FILETYPE fileType) {
		if(fileType == FILETYPE.CSV){
			return TransactionReader.getTrasactionReaderInstance().readXmlFile();
		}
		return null;
	}

}
