package com.sapient.assignment.reader;

import com.sapient.assignment.constant.Constant.FILETYPE;

public interface ITransactionManager {

	public void displayTransactionReport();
	public ITransactionManager readFile(FILETYPE fileType);
}
