package com.sapient.assignment.reader;

import java.io.File;

import com.sapient.assignment.constant.Constant.FILETYPE;



public class TransactionReader {
	/**
	 * 
	 */
	private static TransactionReader trasactionReader;
	/**
	 * 
	 */
	private static ExcelTransactionReader excelTransctionReader;
	/**
	 * 
	 */
	private static XMLTransactionReader xmlTransactionReader;
	/**
	 * 
	 */
	private static TextTransactionReader textTransctionReader;

	/**
	 * 
	 * @return
	 */
	public static TransactionReader getTrasactionReaderInstance() {
		if (null == trasactionReader) {
			synchronized (TransactionReader.class){
				if (null == trasactionReader) {
					trasactionReader = new TransactionReader();
					excelTransctionReader= new ExcelTransactionReader();
					xmlTransactionReader = new XMLTransactionReader();
					textTransctionReader = new TextTransactionReader();
				}
			}
		}
		return trasactionReader;
	}

	/**
	 * 
	 * @return
	 */
	public ExcelTransactionReader readExcelFile(){
		return excelTransctionReader;
	}

	/**
	 * 
	 * @return
	 */
	public XMLTransactionReader readXmlFile(){
		return xmlTransactionReader;
	}

	/**
	 * 
	 * @return
	 */
	public TextTransactionReader readTextFile(){
		return textTransctionReader;
	}

	/**
	 * 
	 * @param fileType
	 * @return
	 */
	public ITransactionManager readFile(FILETYPE fileType,File transactionFile) {
		switch (fileType) {
		case CSV:
			excelTransctionReader.readTransaction(transactionFile);
			return excelTransctionReader;
		case TEXT:
			textTransctionReader.readTransaction(transactionFile);
			return textTransctionReader;
		case XML:
			xmlTransactionReader.readTransaction(transactionFile);
			return xmlTransactionReader;

		default:
			return null;
		}
	}



}
